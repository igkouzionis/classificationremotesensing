import matplotlib.pyplot as plt
import numpy as np
from sklearn.decomposition import PCA
import scipy.io as sio
import os

data = sio.loadmat(os.path.join('/home/jarvis/Desktop/HSI/HSI-Use-Codes/best_models/Salinas_corrected.mat'))['salinas_corrected']
newX = np.reshape(data, (-1, data.shape[2]))
pca = PCA().fit(newX)
plt.figure()
plt.plot(np.cumsum(pca.explained_variance_ratio_))
plt.grid()
plt.xlabel('Spectral Channels')
plt.ylabel('Cumulative Explained Variance')
plt.savefig("SAcumPCA.eps")
plt.show()
