import numpy as np
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
import scipy.io as sio
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
import os
import random
from random import shuffle
from skimage.transform import rotate
import scipy.ndimage
import matplotlib.pyplot as plt
import time
import scipy
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.optimizers import SGD
from keras import backend as K
K.common.set_image_dim_ordering('th')
from keras.utils import np_utils
from keras.utils import plot_model
import h5py
from keras.models import load_model
from keras.utils import np_utils
from sklearn.metrics import classification_report, confusion_matrix, cohen_kappa_score, accuracy_score
import itertools
import spectral
import matplotlib
from operator import truediv
from data_processing import *

dataset = "SA"  # "IP", "PC", "PU", "SA"

windowSize = 7  # 7: IP, SA, 5:PC-PU
numPCAcomponents = 30 if dataset == 'IP' or dataset == "SA" else 15
testRatio = 0.3  # 0.3:IP

# Load dataset Indian Pines
Dataset, GroundTruth = loadData()

# Apply PCA or LDA on dataset
# Dataset,pca = applyPCA(Dataset,numPCAcomponents)
Dataset,lda = applyLDA(Dataset,GroundTruth)

# Create patches from the dataset
XPatches, yPatches = createPatches(Dataset, GroundTruth, windowSize=windowSize)

# Split dataset into train and test set
X_train, X_test, y_train, y_test = splitTrainTestSet(XPatches, yPatches, testRatio)

# Apply over-sampling on the weak classes
X_train, y_train = oversampleWeakClasses(X_train, y_train)

# Apply data augmentation of the training dataset
X_train = AugmentData(X_train)

# Save sets for training and testing
savePreprocessedData(X_train, X_test, y_train, y_test, windowSize = windowSize, wasPCAapplied=False, wasLDAapplied=True, numPCAComponents = numPCAcomponents, testRatio = testRatio)

# X_train = np.load("X_trainPatches_" + str(windowSize) + "PCA" + str(numPCAcomponents) + "testRatio" + str(testRatio)  + ".npy")
# y_train = np.load("y_trainPatches_" + str(windowSize) + "PCA" + str(numPCAcomponents) + "testRatio" + str(testRatio) + ".npy")

X_train = np.load("X_trainPatches_" + str(windowSize) + "LDA" + str(numPCAcomponents) + "testRatio" + str(testRatio)  + ".npy")
y_train = np.load("y_trainPatches_" + str(windowSize) + "LDA" + str(numPCAcomponents) + "testRatio" + str(testRatio) + ".npy")


output_units = 9 if (dataset == 'PU' or dataset == 'PC') else 16


X_train = np.reshape(X_train, (X_train.shape[0],X_train.shape[3], X_train.shape[1], X_train.shape[2]))

# convert class labels to on-hot encoding
y_train = np_utils.to_categorical(y_train)

input_shape= X_train[0].shape
print(input_shape)

# number of filters
C1 = 3*numPCAcomponents

# Define the model
model = Sequential()

model.add(Conv2D(C1, (3, 3), activation='relu', input_shape=input_shape))
model.add(Conv2D(3*C1, (3, 3), activation='relu'))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(6*numPCAcomponents, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(output_units, activation='softmax'))

sgd = SGD(lr=0.0001, decay=1e-6, momentum=0.9, nesterov=True)  # 0.0001
model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])

start =time.time()
history = model.fit(X_train, y_train, batch_size=32, epochs=20)  # 80:IP, 10:PU, 20:PC
end = time.time()
tr_time = end - start
print('Training time: ', end - start)


## Plot Training and Validation loss and Accuracy
plt.figure(figsize=(7,7))
plt.rcParams.update({'font.size': 18})
plt.grid()
plt.plot(history.history['loss'])
plt.ylabel('Loss')
plt.xlabel('Epochs')
plt.title('Training loss')
plt.legend(['Training'], loc='upper right')
plt.savefig(str(dataset) + "_loss_PCA.eps")
plt.show()
## Plot Training and Validation Accuracy
plt.figure(figsize=(7,7))
plt.ylim(0,1.1)
plt.grid()
plt.plot(history.history['acc'])
plt.ylabel('Accuracy')
plt.xlabel('Epochs')
plt.title('Training Accuracy')
plt.legend(['Training'])
plt.savefig(str(dataset) + "_Accuracy_PCA.eps")
plt.show()

# model.save(str(dataset) + '_my_model' + str(windowSize) + 'PCA' + str(numPCAcomponents) + "testRatio" + str(testRatio) + '.h5')
model.save(str(dataset) + '_my_model' + str(windowSize) + 'LDA' + str(numPCAcomponents) + "testRatio" + str(testRatio) + '.h5')
plot_model(model, to_file='myModel.png', show_shapes=True)

X_test  = np.reshape(X_test, (X_test.shape[0], X_test.shape[3], X_test.shape[1], X_test.shape[2]))
y_test = np_utils.to_categorical(y_test)


Y_pred = model.predict(X_test)
y_pred = np.argmax(Y_pred, axis=1)

classification = classification_report(np.argmax(y_test, axis=1), y_pred)
print(classification)

# load the model architecture and weights
# model = load_model(str(dataset) + '_my_model' + str(windowSize) + 'PCA' + str(numPCAcomponents) + "testRatio" + str(testRatio) + '.h5')
model = load_model(str(dataset) + '_my_model' + str(windowSize) + 'LDA' + str(numPCAcomponents) + "testRatio" + str(testRatio) + '.h5')

classification, confusion, Test_loss, Test_accuracy, oa, each_acc, aa, kappa = reports(X_test,y_test, dataset)
classification = str(classification)
confusion = str(confusion)
file_name = str(dataset) + "_classification_report_LDA.txt"

with open(file_name, 'w') as x_file:
    x_file.write('{} Test loss (%)'.format(Test_loss))
    x_file.write('\n')
    x_file.write('{} Test accuracy (%)'.format(Test_accuracy))
    x_file.write('\n')
    x_file.write('\n')
    x_file.write('{} Kappa accuracy (%)'.format(kappa))
    x_file.write('\n')
    x_file.write('{} Overall accuracy (%)'.format(oa))
    x_file.write('\n')
    x_file.write('{} Average accuracy (%)'.format(aa))
    x_file.write('\n')
    x_file.write('\n')
    x_file.write('{}'.format(classification))
    x_file.write('\n')
    x_file.write('{}'.format(confusion))
    x_file.write('{} Training time (s)'.format(tr_time))

# load the original image
Dataset, GroundTruth = loadData()

# Dataset,pca = applyPCA(Dataset,numPCAcomponents)
Dataset,lda = applyLDA(Dataset,GroundTruth)


height = GroundTruth.shape[0]
width = GroundTruth.shape[1]
PATCH_SIZE = windowSize
numPCAcomponents = numPCAcomponents

# calculate the predicted image
outputs = np.zeros((height,width))
for i in range(height-PATCH_SIZE+1):
    for j in range(width-PATCH_SIZE+1):
        target = GroundTruth[int(i+PATCH_SIZE/2), int(j+PATCH_SIZE/2)]
        if target == 0 :
            continue
        else :
            image_patch=Patch(Dataset,i,j)
            X_test_image = image_patch.reshape(1,image_patch.shape[2],image_patch.shape[0],image_patch.shape[1]).astype('float32')                                   
            prediction = (model.predict_classes(X_test_image))                         
            outputs[int(i+PATCH_SIZE/2)][int(j+PATCH_SIZE/2)] = prediction+1

# Plot the Ground Truth Image
ground_truth = spectral.imshow(classes = GroundTruth,figsize =(7,7))
plt.title('Classification map of GroundTruth')
spectral.save_rgb("ground_truth.png", GroundTruth, colors=spectral.spy_colors)

# Plot the Predicted image
predict_image = spectral.imshow(classes = outputs.astype(int),figsize =(7,7),cmap= 'jet')
plt.title('Predicted Classification map by CNN after Dimensionality Reduction')
spectral.save_rgb("IP_predictions_LDA.png", outputs.astype(int), colors=spectral.spy_colors)
